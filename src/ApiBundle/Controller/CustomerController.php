<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;

class CustomerController extends FOSRestController
{
    /**
     * Here goes our route
     * @Get("/get/hello")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function helloGetAction(Request $request)
    {
        // Do something with your Request object
        $data = array(
            "name" => "API",
            "path" => "Hello"
        );
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }
}

<?php
namespace ApiBundle\Listeners;

use ApiBundle\Service\ApiLogger;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class KernelListener
{
    protected $apiLogger;

    public function __construct(ApiLogger $apiLogger) // this is @service_container
    {
        $this->apiLogger = $apiLogger;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->apiLogger->getLogger()->info("onKernelRequest");
        $request   = $event->getRequest()->getContent();
        $this->apiLogger->getLogger()->info($request);
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $this->apiLogger->getLogger()->info("onKernelResponse");
        $response  = $event->getResponse()->getContent();
        $this->apiLogger->getLogger()->info($response);



    }
}